import React, { useEffect, useState } from 'react'
import axios from 'axios'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Input from '../components/input/input'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'

import logo from '../media/logo.png'

function Client (props) {

    let id = props.match.params.id

    let [state, setState] = useState("auth")
    let [code, setCodeFx] = useState("")
    let [displayError, setError] = useState("")
    let [pointInfo, setPointInfo] = useState({})
    let [userInfo, setUserInfo] = useState({})
    let [actions, setActions] = useState([])

    if ((localStorage.getItem('session-token') == null) || (localStorage.getItem('session-token') == "null") ) {
     window.location.href = "/" + id + "/auth"
    }
    //let [logo, setLogo] = useState({})

    function updatePointInfo () {
        axios.get(' https://api4.simployal.com/c/' + id + '/info')
        .then((response) => {
            setPointInfo(response.data)
        })
        .catch((error) => {
            setError(error.message)
        })
    }

    function addVisit () {
        axios.post(' https://api4.simployal.com/p/' + id + '/pay', {
            code: code,
            amount: 1
        }, {
            headers: {
                Authorization: localStorage.getItem('session-token')
            }
        })
        .then((response) => {
            findClient(code)
            let a = actions
            a.push({action: "Зачислен 1 бонус"})
            setActions(a)
        })
        .catch((error) => {
            setError(error.message)
        })
    }

    function removeVisits () {
        axios.post(' https://api4.simployal.com/p/' + id + '/pay', {
            code: code,
            amount: -1 * (pointInfo.type - 1)
        }, {
            headers: {
                Authorization: localStorage.getItem('session-token')
            }
        })
        .then((response) => {
            findClient(code)
            let a = actions
            a.push({action: "Списаны бонусы"})
            setActions(a)
            //setPointInfo(response.data)
        })
        .catch((error) => {
            setError(error.message)
        })
    }

    useEffect(() => {
        updatePointInfo()
        /*axios.get(' https://api4.simployal.com/c/' + id + '/logo/1', {size: "small"})
        .then((response) => {
            alert(JSON.stringify(response.data))
            setLogo(response.data)
        })
        .catch((error) => {
            setError(error.message)
        })*/
    }, [userInfo])

    function setCode (e) {
        let code = e.target.value
        setError("")

        setCodeFx(code)

        if (code.length >= pointInfo.code_length) {
            findClient(code)
        }

        code = code.substring(0, pointInfo.code_length)
    }

    function findClient (client_code) {
        axios.get(' https://api4.simployal.com/c/' + id + '/amount/' + client_code)
        .then((response) => {
            if (response.data.message="ok") {
                setState("client")
                setUserInfo(response.data)
            } else {
                setError(JSON.stringify(response.data.message))
            }
        })
        .catch(function (error) {
            setError(JSON.stringify(error.message))
        })
    }

    return (
        <div className="application">
            {(state == "auth") && <> <img className="img" src={logo}/>
            <View header="Работа с клиентом" title="Готов к работе">
                <Input type="number" pattern="[0-9]*" scale="large" header="Введите идентификатор клиента" onChange={setCode} value={code}  hint="123456" autoFocus={true}/>
                {(displayError != "")&& <>
                    <Text text="Такой пользователь не найден"></Text>
                    <Row nowrap="true" name={displayError}/>
                </>}
            </View> </>}
            {(state == "client") && <> <img className="img" src={logo}/>
            <View header={"Клиент: " + userInfo.name} title="Клиент">
                <Text text={"Текущий баланс: " + userInfo.amount}/>
                
                <Button color="#353DF6" backgroundColor="#D0EBFF" text="Добавить +1" onClick={() => addVisit()}/>
                <Button color="#353DF6" backgroundColor="#D0EBFF" text={"Cписать " + (pointInfo.type - 1) + " за беспл."} onClick={() => removeVisits()}/>
                {(displayError != "")&& <>
                    <Text text="Ошибка в действии"></Text>
                    <Row nowrap="true" name={displayError}/>
                </>}
                <Divider/>
                <Button text="Следующий клиент" onClick={() => {
                    setCodeFx("")
                    setState("auth")
                    setActions([])
                }}/>
                <Divider/>
                {actions.map((el) => 
                    <>
                        <Row nowrap="true" name={el.action}/>
                    </>
                )}
            </View> </>}
        </div>
    );
}

export default Client;
