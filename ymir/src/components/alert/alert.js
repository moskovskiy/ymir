import React from 'react'
import './alert.css'

import Button from '../button/button'

function Alert(props) {
return (
    <>{props.visible && <><button onClick={()=>props.onClose(false)} className="alert__bg"/>
        <div className="alert">
        <div className="alert__emoji">☹️</div>
        <div className="alert__header">{props.header}</div>
        <div className="alert__text">{props.text}</div>
        <br/>
        <Button onClick={()=>props.onClose(false)} text={(props.action)?props.action:"Жаль"} color="#000" backgroundColor="#D1EDFF"/>
    </div></>}</>
);
}
  
export default Alert;
  