import React from 'react'
import './input.css'

function Input(props) {
    return (
        <div className={(props.scale == "large")? "input input--large":"input"}>
            <div className="input__header">{props.header}</div>
            <input pattern={props.pattern} style={{textAlign: (props.center)? "left" : "center"}} autoFocus={props.autoFocus} type={props.type} pattern={props.pattern} placeholder={props.hint} value={props.value} onChange={props.onChange} className="input__field"/>
        </div>
    )
}
  
export default Input;
  