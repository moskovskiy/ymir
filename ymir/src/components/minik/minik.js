import React from 'react'
import './minik.css'

function Button(props) {
return (
    <button className={props.active ? "minik minik--active" : "minik"} onClick={props.onClick} style={{color: props.color, backgroundColor: props.backgroundColor}}>
        {props.text}
    </button>
);
}
  
export default Button;
  