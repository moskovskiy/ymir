import React from 'react'
import './divider.css'

function Divider(props) {
    return (<div style={{borderTopColor: (props.color)?props.color:"#d6d6d6"}} className="divider"/>)
}
  
export default Divider;
  